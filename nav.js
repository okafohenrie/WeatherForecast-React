import React from 'react';

const Nav = ({value}) => ( 
	<div className="nav">
		<nav>{value}</nav>
	</div>
);

export default Nav;